// Aufgabe 4
// Jetzt wird Lotto gespielt.
// In der Klasse „Lotto“ gibt es ein ganzzahliges Array, welches 6 Lottozahlen von 1 bis 49 aufnehmen kann.
// Konkret sind das die Zahlen 3, 7, 12, 18, 37 und 42. Tragen Sie diese im Quellcode fest ein.

//         a) Geben Sie zunaechst schleifenbasiert den Inhalt des Arrays in folgender Form aus: [ 3 7 12 18 37 42 ]

//         b) Pruefen Sie nun nacheinander, ob die Zahlen 12 bzw. 13 in der Lottoziehung vorkommen.
//         Geben Sie nach der Prüfung aus:
//                 -> Die Zahl 12 ist in der Ziehung enthalten.
//                 -> Die Zahl 13 ist nicht in der Ziehung enthalten.


public class Aufgabe4_Lotto {
    public static void main(String[] args) {

        int[] lottozahlen = {3,7,12,18,37,42};  // Array erzeugen mit den entsprechenden Werten

// --------------------------------------------------------------------------------------------------------------------------------
        // Aufgabenteil a
        System.out.print("Lottozahlen lauten: ");

        // Ausgabe des Arrays als String
        for (int i = 0; i < lottozahlen.length;i++){

            if (i == 0){
                System.out.print("[ ");
            }

            System.out.print(lottozahlen[i] + " ");

            if ( i == lottozahlen.length - 1 ){
                System.out.print("]\n");
            }
        }
// --------------------------------------------------------------------------------------------------------------------------------

        // Aufgabenteil b

        // Prüfung Zahl 12
        if (zahlEnthalten(12, lottozahlen) == true) {
			System.out.println("Die Zahl " + 12 + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + 12 + " ist nicht in der Ziehung enthalten.");
		}

        // Prüfung Zahl 13
		if (zahlEnthalten(13, lottozahlen) == true) {
			System.out.println("Die Zahl " + 13 + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + 13 + " ist nicht in der Ziehung enthalten.");
		}

	}

    // Methode zur Prüfung ob Zahl enthalten oder nicht enthalten ist
	public static boolean zahlEnthalten(int x, int[] lotto) {
		boolean status = false;

		for (int i = 0; i < lotto.length; i++) {
			if (lotto[i] == x) {
				status = true;
			}
		}

		return status;
	}








    }



